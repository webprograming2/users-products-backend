import { Injectable, NotFoundException } from '@nestjs/common';
import { CreateProductDto } from './dto/create-product.dto';
import { UpdateProductDto } from './dto/update-product.dto';
import { Product } from './entities/product.entity';
let products: Product[] = [
  { id: 1, name: 'ชานมไข่มุก', price: 45 },
  { id: 2, name: 'โกโก้ปั่น', price: 50 },
  { id: 3, name: 'น้ำเปล่า', price: 10 },
];

let lastId = 4;

@Injectable()
export class ProductsService {
  create(createProductDto: CreateProductDto) {
    const newProduct: Product = {
      id: lastId++,
      ...createProductDto,
    };
    products.push(newProduct);
    return newProduct;
  }

  findAll() {
    return products;
  }

  findOne(id: number) {
    const index = products.findIndex((item) => {
      return item.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    return products[index];
  }

  update(id: number, updateProductDto: UpdateProductDto) {
    const index = products.findIndex((item) => {
      return item.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const updateProduct: Product = {
      ...products[index],
      ...updateProductDto,
    };
    products[index] = updateProduct;
    return updateProduct;
  }

  remove(id: number) {
    const index = products.findIndex((item) => {
      return item.id == id;
    });
    if (index < 0) {
      throw new NotFoundException();
    }
    const removeProduct = products[index];
    products.splice(index, 1);
    return removeProduct;
  }

  reset() {
    products = [
      { id: 1, name: 'ชานมไข่มุก', price: 45 },
      { id: 2, name: 'โกโก้ปั่น', price: 50 },
      { id: 3, name: 'น้ำเปล่า', price: 10 },
    ];

    lastId = 4;
    return 'RESET';
  }
}
